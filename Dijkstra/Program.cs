﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace dijkstra
{
    class Dijkstra_
    {
        // implementation algorithm dijkstra
        public static void Main(string[] args)
        {
            int infinity = int.MaxValue / 3;
            int[,] graph = { {0,10,infinity,5,infinity}
                            ,{infinity,0,1,2,infinity}
                            ,{infinity,infinity,0,infinity,4}
                            ,{infinity,3,9,0,2}
                            ,{7,infinity,6,infinity,0}
                                 };

            Dijkstra(graph, 0, 5);

            Console.ReadKey();
        }
             static void Dijkstra(int[,] graph, int source, int verticesCount)
        {
            int[] distance = new int[verticesCount];
            bool[] shortestPathTreeSet = new bool[verticesCount];

            for (int i = 0; i < verticesCount; ++i)
            {
                distance[i] = int.MaxValue;
                shortestPathTreeSet[i] = false;
            }

            distance[source] = 0;

            for (int i = 0; i < verticesCount - 1; ++i)
            {
                int u = MinimDistance(distance, shortestPathTreeSet, verticesCount);
                shortestPathTreeSet[u] = true;

                for (int j = 0; j < verticesCount; j++)
                    if (!shortestPathTreeSet[j] && Convert.ToBoolean(graph[u, j]) && distance[u] != int.MaxValue && distance[u] + graph[u, j] < distance[j])
                    {
                        distance[j] = distance[u] + graph[u, j];
                    }
            }

            Print(distance, verticesCount);
        }

        static int MinimDistance(int[] distance, bool[] shortestPathTreeSet, int verticesCount)
        {
            int min = int.MaxValue;
            int minIndex = 0;

            for (int i = 0; i < verticesCount; i++)
            {
                if (shortestPathTreeSet[i] == false && distance[i] <= min)
                {
                    min = distance[i];
                    minIndex = i;
                }
            }

            return minIndex;
        }

        static void Print(int[] distance, int verticesCount)
        {
            Console.WriteLine("Vértice    Distancia desde la fuente");

            for (int i = 0; i < verticesCount; ++i)
                Console.WriteLine("{0}\t  {1}", i, distance[i]);
        }
    }
}