﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace RabinKarp
{
    // Text search algorithm (Rabin-Karp)
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("");
            Console.WriteLine("Por favor escriba un texto..");
            string s1 = Console.ReadLine();
            Console.WriteLine("");
            Console.WriteLine("Ahora escriba la palabra que desea buscar..");
            Console.WriteLine("");
            string s2 = Console.ReadLine();
            string resultado = Rabin_Karp(s1, s2);
            Console.WriteLine("");
            Console.WriteLine(resultado);

            Console.ReadKey();
        }
        static string Rabin_Karp(string s1, string s2)
        {
            string result = "";
            int aux = 0;
            ulong hash_a = 0; // se inican en el mismo valor
            ulong hash_b = 0;
            ulong rolling = 1;  // necesario para el hash rodante
            ulong prime = 100007; // num primo muy grande (num magico)
            ulong base_ = 256; // num de 8 bits, ideal para la comparacion (num magico)

            for (int i = 0; i < s2.Length; i++)
            {
                hash_a = (hash_a * base_ + (ulong)s1[i]) % prime;
                hash_b = (hash_b * base_ + (ulong)s2[i]) % prime;
            }

            if (hash_a == hash_b)
            {
                string print1 = s1.Substring(0, s2.Length);
                string print2 = s1.Substring(s2.Length);

                result += $"Se Encontro una coincidencia en la posición [{hash_a}], >>{print1}<< {print2}";
                aux++;
            }

            for (int i = 1; i <= s2.Length - 1; i++)
            {
                rolling = (rolling * base_) % prime;
            }

            for (int j = 1; j <= s1.Length - s2.Length; j++)
            {
                hash_a = (hash_a + prime - rolling * (ulong)s1[j - 1] % prime) % prime;
                hash_a = (hash_a * base_ + (ulong)s1[j + s2.Length - 1]) % prime;

                if (hash_b == hash_a)
                {
                    if (s1.Substring(j, s2.Length) == s2)
                    {

                        string print1 = s1.Substring(0, j);
                        string print2 = s1.Substring(j, s2.Length);
                        string print3 = s1.Substring(j + s2.Length);

                        result += $"\n Se Encontro una coincidencia en la posición [{j}],  {print1} >>{print2}<< {print3} ";
                        aux++;
                    }
                }
            }

            if (aux > 0)
            {
                return result;
            }
            else
            {
                return result = $"No se encontro el valor({s2}) en el texto ({s1})";
            }
        }

    }
}