﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bfs
{ 
    //algorithm search graphs using a BFS
    public class BFS_
    {
        static void Main(string[] args)
        {   // num de vertices que va a tener el grafo
            int totalVertix = 7;
            BreadthFirstSearch bfs = new BreadthFirstSearch(totalVertix);
            // se agregan los vertices y se crean las aristas que van a unir el nodo a con el nodo b
            bfs.AddVertex(1, 2);
            bfs.AddVertex(1, 5);
            bfs.AddVertex(2, 5);
            bfs.AddVertex(2, 3);
            bfs.AddVertex(3, 6);
            bfs.AddVertex(4, 5);
            bfs.AddVertex(4, 3);
            bfs.AddVertex(5, 6);
            // se compara la ruta mas optima para llegar de un nodo a otro   
            bfs.Comparator(1, 6);

            Console.ReadLine();
        }
        public class Vertex
        {
            public Vertex(int val)
            {
                vertex = val;
                distance = Int32.MaxValue;
                color = 0;
            }
            public int vertex { get; set; }
            public Vertex next { get; set; }
            public int distance { get; set; }
            public int color { get; set; }
            public Vertex pi { get; set; }
        }

        public class BreadthFirstSearch
        {
            public int v;
            public Vertex[] adjList;
            public BreadthFirstSearch(int totalVertix)
            {
                v = totalVertix;
                adjList = new Vertex[totalVertix];
                for (int i = 0; i < adjList.Length; i++)
                    adjList[i] = new Vertex(i);

            }

            public void AddVertex(int u, int v)
            {
                Vertex tempU = adjList[u];
                while (tempU.next != null)
                {
                    if (tempU.vertex != v)
                        tempU = tempU.next;
                    else
                        return;
                }
                tempU.next = new Vertex(v);

                Vertex tempV = adjList[v];
                while (tempV.next != null)
                    tempV = tempV.next;

                tempV.next = new Vertex(u);
            }
            public void Comparator(int u, int v)
            {
                BFS(u);
                Print(adjList[u], adjList[v]);
            }
            public void Print(Vertex u, Vertex v)
            {
                if (v == u)
                {
                    Console.WriteLine($"Nodo en ruta  ({u.vertex})  ");
                }
                else if (v.pi == null)
                {
                    Console.WriteLine($"No hay comunicacion, nodo no existe");
                }
                else
                {
                    Print(u, v.pi);
                    Console.WriteLine($"Nodo en ruta  ({v.vertex})  ");
                }
            }
            public void BFS(int source)
            {
                Queue<Vertex> queue = new Queue<Vertex>();
                Vertex src = adjList[source];
                src.color = 1;
                src.distance = 0;
                src.pi = null;

                for (int i = 0; i < adjList.Length; i++)
                {
                    Vertex u = adjList[i];
                    if (u.vertex != source)
                    {
                        u.color = 0; // inicial white
                        u.distance = Int32.MaxValue;
                        u.pi = null;
                    }
                }

                queue.Enqueue(src);
                while (queue.Count > 0)
                {
                    Vertex u = queue.Dequeue();
                    Vertex v = u.next;
                    while (v != null)
                    {
                        Vertex mainV = adjList[v.vertex];
                        if (mainV.color == 0)  // = white
                        {
                            mainV.color = 1; //grey 
                            mainV.distance = u.distance + 1;
                            mainV.pi = u;
                            queue.Enqueue(mainV);
                        }
                        v = v.next;
                    }
                    u.color = 2; //black
                }
            }
        }
    }
}