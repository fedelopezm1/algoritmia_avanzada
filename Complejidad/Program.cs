﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Complejidad
{
    //Para cada función f(n) y tiempo t en la siguiente tabla, determine la tamaño más grande n de un problema que puede
    //ser solucionado en el tiempo t.Asumiendo que el algoritmo para solucionar el problema toma f(n) microsegundos.
    class Program
    {
        static void Main(string[] args)
        { // segundos
            int N = 1000000; // micro segundos 
            double Cuadratica_N = Cuadratica(N);
            int N_Log_N = Log(N);
            int Factorial_N = Factorial(N);
            double Exponencial_N = Exponencial(N);
            double Cubica_N = Cubica(N);
            Console.WriteLine("Segundos:");
            Console.WriteLine("");
            Console.WriteLine("");
            Console.WriteLine($"Log n = 10^10^6, (al calcucar en c# se desbordaba por su tamaño)");
            Console.WriteLine("");
            Console.WriteLine($"Raiz: n = 1*10^12 (al calcucar en c# se desbordaba por su tamaño)");
            Console.WriteLine("");
            Console.WriteLine($"Micro Segundos: n = {N}");
            Console.WriteLine("");
            Console.WriteLine($"Logatirmica: n log n = {N_Log_N}");
            Console.WriteLine("");
            Console.WriteLine($"Cuadratica:  n^2 = {Cuadratica_N}");
            Console.WriteLine("");
            Console.WriteLine($"Cubica: n^3 = {Cubica_N}");
            Console.WriteLine("");
            Console.WriteLine($"Exponencial 2^n = {Exponencial_N}");
            Console.WriteLine("");
            Console.WriteLine($"Factoail n! = {Factorial_N}");
            Console.ReadKey();
        }

        static int Log(int N)
        {
            int aux = 0;
            for (int i = 0; i < N; i++)
            {
                double log = Math.Log(i, 10);
                double result = i * log;

                if (result >= N)
                {
                    aux = i - 1;
                    return aux;
                }
            }
            return aux;
        }
        static double Cuadratica(int N)
        {
            double result = 0;
            for (int i = 0; i < N; i++)
            {
                result = Math.Pow(i, 2);

                if (result > N)
                {
                    result = i -1;
                    return result;
                }
            }
            return result;
        }
        static double Cubica(int N)
        {
            double result = 0;
            for (int i = 0; i < N; i++)
            {
                 result = Math.Pow(i, 3);

                if (result > N)
                {
                    result = i -1;
                    return result;
                }
            }
            return result;
        }
        static int Factorial(int N)
        {
            int result = 1;
            for (int i = 1; i < N; i++)
            {
                result = result * i;
                if (result >= N)
                {
                    result = i - 1;
                    return result;
                }
            }
            return result;
        }

        static double Exponencial(int N)
        {
            double result = 1;
            for (int i = 1; i < N; i++)
            {
                result = Math.Pow(2,i);
                if (result >= N)
                {
                    result = i - 1;
                    return result;
                }
            }
            return result;
        }

    }
}
