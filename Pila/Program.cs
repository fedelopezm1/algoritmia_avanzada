﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pila_
{
    class Pila
    {
        class Program
        {
            static void Main(string[] args)
            {
                int opcion = 0;
                Pila pila = new Pila();
                int nodo;
                do
                {
                    opcion = Menu_Principal();
                    switch (opcion)
                    {
                        case 1:
                            Console.Write("Ingrece el Valor del Nuevo Nodo: ");
                            nodo = int.Parse(Console.ReadLine());
                            pila.addNodo(nodo);
                            Console.Clear();
                            break;
                        case 2:
                            if (pila.Raiz != null)
                            {
                                string viewPila = pila.ViewNodos();
                                Console.WriteLine("Nodo en la Pila");
                                Console.WriteLine("");
                                Console.WriteLine(viewPila);
                                Thread.Sleep(3000);
                            }
                            else
                            {
                                Console.Write("Pila Vacia, Validar....");
                                Thread.Sleep(3000);
                            }
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 3:
                            if (pila.Raiz != null)
                            {
                                string viewPila = pila.ViewNodosEveryone();
                                Console.WriteLine("Todos los nodos en la Pila");
                                Console.WriteLine("");
                                Console.WriteLine(viewPila);
                                Thread.Sleep(3000);
                            }
                            else
                            {
                                Console.Write("Pila Vacia, Validar....");
                                Thread.Sleep(3000);
                            }
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 4:
                            if (pila.Raiz != null)
                            {
                                pila.deleteNodo();
                                Console.WriteLine("Eliminando....");
                                Console.WriteLine("");
                                Console.WriteLine("Todos los nodos en la Pila");
                                Console.WriteLine("");
                                string viewPila = pila.ViewNodosEveryone();
                                Console.Write(viewPila);
                                Thread.Sleep(3000);
                            }
                            else
                            {
                                Console.WriteLine("Pila Vacia, Validar....");
                                Thread.Sleep(3000);
                            }
                            Console.ReadLine();
                            Console.Clear();
                            break;
                        case 5:
                            finish();
                            break;
                    }
                } while (opcion != 5);

                Console.ReadKey();
            }
            static int Menu_Principal()
            {
                int result = 0;
                do
                {
                    Console.WriteLine("Menú Pilas");
                    Console.WriteLine("");
                    Console.WriteLine(" 1. Insertar un Nuevo Nodo a la Pila");
                    Console.WriteLine(" 2. Mirar Nodo en la Pila");
                    Console.WriteLine(" 3. Mirar todos los nodos de la Pila");
                    Console.WriteLine(" 4. Eliminar un Nodo de la Pila");
                    Console.WriteLine(" 5. Finalizar");
                    Console.WriteLine("");
                    Console.Write("Oprima la Opcion Deseada Realizar: ");
                    var enter = Console.ReadLine();
                    if (enter == "")
                        continue;

                    result = int.Parse(enter);
                    Console.WriteLine("");
                    if (result < 1 || result > 5)
                    {
                        Console.WriteLine("Opcion Invalida....");
                        Console.ReadLine();
                        Console.WriteLine("");
                    }
                    Console.Clear();
                } while (result < 1 || result > 5);
                return result;
            }
            static void finish()
            {
                Console.WriteLine("Finalizando..");
                Console.ReadLine();
            }
            public class Pila
            {
                public class Nodo
                {
                    public int Valor { set; get; }
                    public Nodo Siguiente { set; get; }
                }
                public Nodo Raiz;
                public Pila()
                {
                    Raiz = null;
                }
                public string ViewNodos()
                {
                    Nodo Nodos = Raiz;

                    string pila = $"({Nodos.Valor})";
                    Nodos = Nodos.Siguiente;

                    return pila;
                }
                public string ViewNodosEveryone()
                {
                    string pila = "";
                    Nodo Nodos = Raiz;
                    while (Nodos != null)
                    {
                        pila += $"({Nodos.Valor}) \n";
                        Nodos = Nodos.Siguiente;
                    }

                    return pila;
                }
                public void addNodo(int nodo)
                {
                    Nodo Nuevo = new Nodo();
                    Nuevo.Valor = nodo;
                    Nuevo.Siguiente = null;

                    if (Raiz != null)
                    {
                        Nuevo.Siguiente = Raiz;
                    }
                    else
                    {
                        Nuevo.Siguiente = null;
                    }
                    Raiz = Nuevo;
                }
                public int deleteNodo()
                {
                    if (Raiz == null)
                    {
                        return int.MaxValue;
                    }
                    else
                    {
                        int Info = Raiz.Valor;
                        Raiz = Raiz.Siguiente;
                        return Info;
                    }
                }
            }
        }
    }
}
