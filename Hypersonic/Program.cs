﻿using System;
using System.Linq;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
public class Player
{
    public static void Main(string[] args)
    {
        string[] inputs;
        inputs = Console.ReadLine().Split(' ');
        int width = int.Parse(inputs[0]);
        int height = int.Parse(inputs[1]);
        int myId = int.Parse(inputs[2]);

        Box box = new Box();
        box.Px = -1;
        box.Py = -1;

        // game loop
        while (true)
        {
            Target target = new Target();
            target.Px = 0;
            target.Py = 0;
            Hero hero = new Hero();
            hero.Pos_x = 0;
            hero.Pos_y = 0;
            int min_dict = int.MaxValue;

            char[,] grid = new char[11, 13];
            for (int i = 0; i < height; i++)
            {
                string row = Console.ReadLine();
                for (int j = 0; j < row.Length; j++)
                {
                    grid[i, j] = row[j];
                }
            }

            if (box.Px != -1)
            {
                grid[box.Py, box.Px] = '.';
            }

            int entities = int.Parse(Console.ReadLine());
            for (int i = 0; i < entities; i++)
            {
                inputs = Console.ReadLine().Split(' ');
                int entityType = int.Parse(inputs[0]);
                int owner = int.Parse(inputs[1]);
                int x = int.Parse(inputs[2]);
                int y = int.Parse(inputs[3]);
                int param1 = int.Parse(inputs[4]);
                int param2 = int.Parse(inputs[5]);

                if (entityType == 0 && owner == myId)
                {
                    hero.Type = entityType;
                    hero.Pos_x = x;
                    hero.Pos_y = y;
                    hero.Owner = owner;
                    //  Console.Error.WriteLine($"desde la condicion hero x {hero.Pos_x}, hero y {hero.Pos_y}");
                }
            }
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < 13; j++)
                {
                    if (grid[i, j] != '.')
                    {
                        int distance = Calculatedistance(hero.Pos_x, hero.Pos_y, j, i);
                        if (distance < min_dict)
                        {
                          //  Console.Error.WriteLine($"distancia = {distance}");
                            min_dict = distance;
                            target.Px = j;
                            target.Py = i;
                        }
                    }
                }
            }
            // Console.Error.WriteLine($"hero x {hero.Pos_x}, hero y {hero.Pos_y}");

            int distan = Calculatedistance(hero.Pos_x, hero.Pos_y, target.Px, target.Py);

            if (distan == 1)
            {
                box.Px = target.Px;
                box.Py = target.Py;

                Console.WriteLine($"BOMB {target.Px} {target.Py}");
                Console.WriteLine($"BOMB {target.Px + 1} {target.Py + 1}");
            }
            else
            {
                Console.WriteLine($"MOVE {target.Px} {target.Py}");
            }
            // Write an action using Console.WriteLine()
            // To debug: Console.Error.WriteLine("Debug messages...");
        }
    }
    static int Calculatedistance(int x1, int y1, int x2, int y2)
    {
        int abs = Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
        return abs;
    }

    public class Hero
    {
        public int Type { get; set; }
        public int Owner { get; set; }
        public int Pos_x { get; set; }
        public int Pos_y { get; set; }
        public Hero()
        {
        }
    }
    public class Box
    {
        public int Px { get; set; }
        public int Py { get; set; }
        public Box()
        {

        }
    }
    public class Target
    {
        public int Px { get; set; }
        public int Py { get; set; }
        public Target()
        {

        }
    }
}