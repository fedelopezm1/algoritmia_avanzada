﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Cola
{
    // Implementar las operaciones de la cola: Encolar (Enqueue), Desencolar(Dequeue) y Mirar(Peek)
    class Program
    {
        static void Main(string[] args)
        {
            int opcion = 0;
            Cola cola = new Cola();
            int nodo;
            do
            {
                opcion = Menu_Principal();
                switch (opcion)
                {
                    case 1:
                        Console.Write("Ingrece el Valor del Nuevo Nodo: ");
                        nodo = int.Parse(Console.ReadLine());
                        cola.Encolar(nodo);
                        Console.Clear();
                        break;
                    case 2:
                        if (cola.Raiz != null)
                        {
                            string viewCola = cola.Mirar();
                            Console.WriteLine("Nodo en la Cola");
                            Console.WriteLine("");
                            Console.WriteLine(viewCola);
                            Thread.Sleep(3000);
                        }
                        else
                        {
                            Console.Write("Cola Vacia, Validar....");
                            Thread.Sleep(3000);
                        }
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 3:
                        if (cola.Raiz != null)
                        {
                            string viewCola = cola.MirarTodo();
                            Console.WriteLine("Nodos en la Cola");
                            Console.WriteLine("");
                            Console.WriteLine(viewCola);
                            Thread.Sleep(3000);
                        }
                        else
                        {
                            Console.Write("Cola Vacia, Validar....");
                            Thread.Sleep(3000);
                        }
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 4:
                        if (cola.Raiz != null)
                        {
                            cola.Desencolar();
                            Console.WriteLine("Desencolando....");
                            Console.WriteLine("");
                            Console.WriteLine("Nodos en la Cola");
                            Console.WriteLine("");
                            string viewCola = cola.MirarTodo();
                            Console.Write(viewCola);
                            Thread.Sleep(3000);
                        }
                        else
                        {
                            Console.WriteLine("Cola Vacia, Validar....");
                            Thread.Sleep(3000);
                        }
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 5:
                        finish();
                        break;
                }
            } while (opcion != 5);

            Console.ReadKey();
        }
        static int Menu_Principal()
        {
            int result = 0;
            do
            {
                Console.WriteLine("Menú Colas");
                Console.WriteLine("");
                Console.WriteLine(" 1. Insertar un Nuevo Nodo a la Cola");
                Console.WriteLine(" 2. Mirar nodo en Cola");
                Console.WriteLine(" 3. Mirar todos los nodos en la Cola");
                Console.WriteLine(" 4. Desencolar un Nodo");
                Console.WriteLine(" 5. Finalizar");
                Console.WriteLine("");
                Console.Write("Oprima la Opcion Deseada Realizar: ");
                var enter = Console.ReadLine();
                if (enter == "")
                    continue;

                result = int.Parse(enter);
                Console.WriteLine("");
                if (result < 1 || result > 5)
                {
                    Console.WriteLine("Opcion Invalida....");
                    Console.ReadLine();
                    Console.WriteLine("");
                }
                Console.Clear();
            } while (result < 1 || result > 5);
            return result;
        }
        static void finish()
        {
            Console.WriteLine("Finalizando..");
            Console.ReadLine();
        }
        public class Cola
        {
            public class Nodo
            {
                public int Valor { set; get; }
                public Nodo Siguiente { set; get; }
            }
            public Nodo Raiz, Final;
            public Cola()
            {
                Raiz = null;
                Final = null;
            }
            public bool Estado()
            {
                if (Raiz != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            public string Mirar()
            {
                Nodo Nodos = Raiz;

                string cola = $"({Nodos.Valor})";
                Nodos = Nodos.Siguiente;

                return cola;
            }
            public string MirarTodo()
            {
                string cola = "";
                Nodo Nodos = Raiz;

                while (Nodos != null)
                {
                    cola += $"({Nodos.Valor})";
                    Nodos = Nodos.Siguiente;
                }
                return cola;
            }
            public void Encolar(int nodo)
            {
                Nodo Nuevo = new Nodo();
                Nuevo.Valor = nodo;
                Nuevo.Siguiente = null;
                bool estado = Estado();

                if (estado == false)
                {
                    Final.Siguiente = Nuevo;
                    Final = Nuevo;
                }
                else
                {
                    Raiz = Nuevo;
                    Final = Nuevo;
                }
            }
            public int Desencolar()
            {
                bool estado = Estado();
                if (estado == false)
                {
                    int informacion = Raiz.Valor;
                    if (Raiz == Final)
                    {
                        Raiz = null;
                        Final = null;
                    }
                    else
                    {
                        Raiz = Raiz.Siguiente;
                    }
                    return informacion;
                }
                else
                    return int.MaxValue;
            }
        }
    }
}