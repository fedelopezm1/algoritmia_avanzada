﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace levinshtein
{
    // algorithm levinshtein Distance
    class Levinshtein
    {
        static void Main(string[] args)
        {
            string A = "CASOTA";
            string B = "ZAPOTE";

            LevenshteinDistance(A, B);

            Console.ReadKey();
        }

        static void LevenshteinDistance(string A, string B)
        {
            int N = A.Length;
            int M = B.Length;
            int[,] D = new int[N + 1, M + 1];

            // fill initial values for row i = 0
            for (int i = 0; i <= M; i++)
            {
                D[0, i] = i;
            }

            // fill initial values for row j = 0
            for (int i = 0; i <= N; i++)
            {
                D[i, 0] = i;
            }

            // solve edit solution solution
            for (int i = 1; i <= N; i++)
            {
                for (int j = 1; j <= M; j++)
                {
                    int cost = 1;
                    if (A[i - 1] == B[j - 1])
                    {
                        cost = 0;
                    }
                    D[i, j] = Math.Min(Math.Min(D[i, j - 1] + 1, D[i - 1, j] + 1), D[i - 1, j - 1] + cost);

                }
            }

            // print matrix
            for (int i = 0; i <= N; i++)
            {
                for (int j = 0; j <= M; j++)
                {
                    Console.Write(D[i, j] + " ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("");
            Console.WriteLine($"La distancia es : {D[N, M]}");
            Console.WriteLine("");

            // print solution inverse
            int I = N;
            int J = M;
            while (I > 0 && J > 0)
            {
                if (D[I, J] == D[I - 1, J] + 1)
                { // delete
                    I--;
                    Console.WriteLine("");
                    Console.WriteLine($"Eliminado: Su origen es lado superior:  {D[I, J]}");
                    Console.WriteLine(D[I, J]);
                }

                else if (D[I, J] == D[I, J - 1] + 1) 
                { // insert
                    J--;
                    Console.WriteLine("");
                    Console.WriteLine($"Insertado: Su origen es del lado izquierdo:  {D[I, J]}");
                }
                else
                { // change
                    I--;
                    J--;
                    Console.WriteLine("");
                    Console.WriteLine($"Cambio: Su origen es del lado diagonal:  {D[I, J]}");
                }

            }

        }
    }
}