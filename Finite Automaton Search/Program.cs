﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Automaton
{
    //text search algorithm using a finite automaton
    class Finite_Automaton
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("");
            Console.WriteLine("Por favor escriba un texto..");
            string text = Console.ReadLine();
            Console.WriteLine("");
            Console.WriteLine("Ahora escriba los caracteres que desea buscar..");
            Console.WriteLine("");
            string text_search = Console.ReadLine();
            char[] arr_text = text.ToCharArray();
            char[] arr_search = text_search.ToCharArray();

            string result = automatonSearch(arr_search, arr_text);
            Console.WriteLine(result);

            Console.ReadKey();
        }
        public static string automatonSearch(char[] arr_s1, char[] arr_s2)
        {
            string result = "";
            int size_s1 = arr_s1.Length;
            int size_s2 = arr_s2.Length;
            int size_primo = 256;  //num magic (bit)
            int size_equal = size_s1 + 1;

            int[][] new_array = new int[size_equal][];

            for (int i = 0; i < size_equal; i++)
            {
                new_array[i] = new int[size_primo];
            }

            for (int i = 0; i <= size_s1; ++i)
            {
                for (int j = 0; j < size_primo; ++j)
                {
                    new_array[i][j] = newState(arr_s1, size_s1, i, j);
                }
            }
            // new estate
            int state_fin = 0;
            int banner = 0;
            for (int i = 0; i < size_s2; i++)
            {
                state_fin = new_array[state_fin][arr_s2[i]];
                if (state_fin == size_s1)
                {
                    result += $"Se encontraron coincidencias en la posición: [{(i - size_s1 + 1)}] \n";
                    banner++;
                }
            }

            if (banner > 0)
            {
                return result;
            }
            else
            {
                return result = $"No se encontraron coincidencias en el texto";
            }
        }
        public static int newState(char[] arr_s1, int size_s1, int state, int position_j)
        {  // validate state position matriz
            if (state < size_s1 && (char)position_j == arr_s1[state])
            {
                return state + 1;
            }

            int h;

            for (int k = state; k > 0; k--)
            {
                if (arr_s1[k - 1] == (char)position_j)
                {
                    for (h = 0; h < k - 1; h++)
                    {
                        if (arr_s1[h] != arr_s1[state - k + 1 + h])
                        {
                            break;
                        }
                    }

                    if (h == k - 1)
                    {
                        return k;
                    }
                }
            }

            return 0;

        }

    }
}
