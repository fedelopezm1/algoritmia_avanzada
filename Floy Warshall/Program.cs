﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace floy_warshall
{
    class Floy_warshall
    {
        public static void Main(string[] args)
        {
            int infinity = int.MaxValue / 3;
            int[,] m = new int[,] { {0,10,infinity,5,infinity}
                                       ,{infinity,0,1,2,infinity}
                                       ,{infinity,infinity,0,infinity,4}
                                       ,{infinity,3,9,0,2}
                                       ,{7,infinity,6,infinity,0}
                                 };
            floy(m);
            Console.ReadKey();
        }

        static void floy(int[,] m)
        {
            int size_row = m.GetLength(0);
            for (int k = 0; k < size_row; k++)
            {
                for (int i = 0; i < size_row; i++)
                {
                    for (int j = 0; j < size_row; j++)
                    {

                        if (m[i, j] > m[i, k] + m[k, j])
                        {
                            m[i, j] = m[i, k] + m[k, j];
                        }
                    }

                }
            }
            // print matrix
            for (int i = 0; i < size_row; i++)
            {
                for (int j = 0; j < size_row; j++)
                {
                    Console.Write($"{m[i, j]}  ");
                }
                Console.WriteLine();
            }
        }
    }
}
