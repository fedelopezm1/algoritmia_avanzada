﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Arbol
{
    class Program
    {
        static void Main(string[] args)
        {
            int opcion = 0;
            Nodo Raiz = null;
            int Dato;
            do
            {
                opcion = Menu_Principal();
                switch (opcion)
                {
                    case 1:
                        Console.Write("Ingrece el Valor del Nuevo Nodo: ");
                        Dato = int.Parse(Console.ReadLine());
                        if (Raiz == null)
                        {
                            Nodo NuevoNodo = new Nodo();
                            NuevoNodo.Informacion = Dato;
                            Raiz = NuevoNodo;
                        }
                        else
                        {
                            Insertar(Raiz, Dato);
                        }
                        Console.Clear();
                        break;

                    case 2:
                        Console.Write("Ingrece el Valor del Nodo a Buscar: ");
                        Dato = int.Parse(Console.ReadLine());
                        if (Raiz != null)
                        {
                            Buscar(Raiz, Dato);
                        }
                        else
                        {
                            Console.WriteLine("El Nodo No Existe en el Arbol, Validar....");
                            Thread.Sleep(3000);
                        }
                        Console.Clear();
                        break;
                    case 3:
                        if (Raiz != null)
                        {
                            Buscar_Max(Raiz);
                        }
                        else
                        {
                            Console.WriteLine("Arbol Vacio, Validar....");
                            Thread.Sleep(3000);
                        }
                        Console.Clear();
                        break;
                    case 4:
                        if (Raiz != null)
                        {
                            Buscar_Min(Raiz);
                        }
                        else
                        {
                            Console.WriteLine("Arbol Vacio, Validar....");
                            Thread.Sleep(3000);
                        }
                        Console.Clear();
                        break;
                    case 5:
                        Preorden(Raiz);
                        Console.WriteLine(" Fin del Arbol..");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 6:
                        Postorden(Raiz);
                        Console.WriteLine(" Fin del Arbol..");
                        Console.ReadLine();
                        Console.Clear();
                        break;

                    case 7:
                        Inorden(Raiz);
                        Console.WriteLine(" Fin del Arbol..");
                        Console.ReadLine();
                        Console.Clear();
                        break;
                    case 8:
                        Console.Write("Ingrece el Valor del nodo a Eliminar: ");
                        Dato = int.Parse(Console.ReadLine());
                        if (Raiz != null)
                        {
                            Eliminar(ref Raiz, Dato);
                        }
                        else
                        {
                            Console.WriteLine("El Nodo a Eliminar no Existe, Validar....");
                            Thread.Sleep(3000);
                        }
                        Console.Clear();
                        break;
                    case 9:
                        Finalizar();
                        break;
                }
            } while (opcion != 9);

        }
        static int Menu_Principal()
        {
            int result = 0;
            do
            {
                Console.WriteLine("Menú Arbol de Busqueda");
                Console.WriteLine("");
                Console.WriteLine(" 1. Insertar un Nuevo Nodo");
                Console.WriteLine(" 2. Buscar un Nodo");
                Console.WriteLine(" 3. Buscar el Mayor Nodo");
                Console.WriteLine(" 4. Buscar el Menor Nodo");
                Console.WriteLine(" 5. Ver Pre-orden");
                Console.WriteLine(" 6. Ver Post-orden");
                Console.WriteLine(" 7. Ver In-orden");
                Console.WriteLine(" 8. Eliminar un Nodo");
                Console.WriteLine(" 9. Finalizar");
                Console.WriteLine("");
                Console.Write("Oprima la Opcion Deseada Realizar: ");
                result = int.Parse(Console.ReadLine());
                Console.WriteLine("");
                if (result < 1 || result > 9)
                {
                    Console.WriteLine("Opcion Invalida....");
                    Console.ReadLine();
                    Console.WriteLine("");
                }
                Console.Clear();
            } while (result < 1 || result > 9);
            return result;
        }
        static void Insertar(Nodo Raiz, int Dato)
        {
            if (Dato < Raiz.Informacion)
            {
                if (Raiz.Nodo_Izquierdo == null)
                {
                    Nodo NuevoNodo = new Nodo();
                    NuevoNodo.Informacion = Dato;
                    Raiz.Nodo_Izquierdo = NuevoNodo;
                }
                else
                {
                    //recursividad
                    Insertar(Raiz.Nodo_Izquierdo, Dato);
                }
            }
            else// buscar por el lado derecho
            {
                if (Dato > Raiz.Informacion)
                {
                    if (Raiz.Nodo_Derecho == null)
                    {
                        Nodo NuevoNodo = new Nodo();
                        NuevoNodo.Informacion = Dato;
                        Raiz.Nodo_Derecho = NuevoNodo;
                    }
                    else
                    {
                        //llamada recursiva por el lado derecho
                        Insertar(Raiz.Nodo_Derecho, Dato);
                    }
                }
                else
                {
                    // el nodo existe en el Arbol
                    Console.WriteLine("El Nodo ya Existe, Validar..");
                    Console.ReadLine();
                }
            }
        }
        static void Buscar(Nodo Raiz, int Dato)
        {
            if (Dato < Raiz.Informacion)
            {
                //buscar por el Sub-Arbol izquierdo
                if (Raiz.Nodo_Izquierdo == null)
                {
                    Console.WriteLine("No se encuentra el Nodo, Validar...");
                    Console.ReadLine();
                }
                else
                { // recursividad
                    Buscar(Raiz.Nodo_Izquierdo, Dato);
                }
            }
            else
            {
                if (Dato > Raiz.Informacion)
                {
                    //buscar por el Sub-Arbol derecho
                    if (Raiz.Nodo_Derecho == null)
                    {
                        Console.WriteLine("No se encuentra el Nodo, Validar...");
                        Console.ReadLine();
                    }
                    else
                    {
                        //recursividad
                        Buscar(Raiz.Nodo_Derecho, Dato);
                    }
                }
                else
                {
                    Console.WriteLine("Nodo Localizado en el Arbol...");
                    Console.ReadLine();
                }
            }
        }
        static void Buscar_Max(Nodo Raiz)
        {
            if (Raiz.Nodo_Derecho == null)
            {
                Console.WriteLine(Raiz.Informacion);
                Console.ReadLine();
            }
            else
            {
                Buscar_Max(Raiz.Nodo_Derecho);
            }
        }
        static void Buscar_Min(Nodo Raiz)
        {
            if (Raiz.Nodo_Izquierdo == null)
            {
                Console.WriteLine(Raiz.Informacion);
                Console.ReadLine();
            }
            else
            {
                Buscar_Min(Raiz.Nodo_Izquierdo);
            }
        }
        static void Preorden(Nodo Raiz)
        {
            if (Raiz != null)
            {
                Console.Write("{0}, ", Raiz.Informacion);
                Preorden(Raiz.Nodo_Izquierdo);
                Preorden(Raiz.Nodo_Derecho);
            }
        }
        static void Inorden(Nodo Raiz)
        {
            if (Raiz != null)
            {
                Inorden(Raiz.Nodo_Izquierdo);
                Console.Write("{0}, ", Raiz.Informacion);
                Inorden(Raiz.Nodo_Derecho);
            }
        }
        static void Postorden(Nodo Raiz)
        {
            if (Raiz != null)
            {
                Postorden(Raiz.Nodo_Izquierdo);
                Postorden(Raiz.Nodo_Derecho);
                Console.Write("{0}, ", Raiz.Informacion);
            }
        }
        static void Eliminar(ref Nodo Raiz, int Dato)
        {
            if (Raiz != null)
            {
                if (Dato < Raiz.Informacion)
                {
                    Eliminar(ref Raiz.Nodo_Izquierdo, Dato);
                }
                else
                {
                    if (Dato > Raiz.Informacion)
                    {
                        Eliminar(ref Raiz.Nodo_Derecho, Dato);
                    }
                    else
                    {
                        //Si lo Encontro
                        Nodo NodoEliminar = Raiz;
                        if (NodoEliminar.Nodo_Derecho == null)
                        {
                            Raiz = NodoEliminar.Nodo_Izquierdo;
                        }
                        else
                        {
                            if (NodoEliminar.Nodo_Izquierdo == null)
                            {
                                Raiz = NodoEliminar.Nodo_Derecho;
                            }
                            else
                            {
                                Nodo AuxiliarNodo = null;
                                Nodo Auxiliar = Raiz.Nodo_Izquierdo;
                                bool Bandera = false;
                                while (Auxiliar.Nodo_Derecho != null)
                                {
                                    AuxiliarNodo = Auxiliar;
                                    Auxiliar = Auxiliar.Nodo_Derecho;
                                    Bandera = true;
                                }
                                Raiz.Informacion = Auxiliar.Informacion;
                                NodoEliminar = Auxiliar;
                                if (Bandera == true)
                                {
                                    AuxiliarNodo.Nodo_Derecho = Auxiliar.Nodo_Izquierdo;
                                }
                                else
                                {
                                    Raiz.Nodo_Izquierdo = Auxiliar.Nodo_Izquierdo;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("EL Nodo no se Encuentra en el Arbol, Validar...");
                Console.ReadLine();
            }
        }
        static void Finalizar()
        {
            Console.WriteLine("Finalizando..");
            Console.ReadLine();
        }
    }
    class Nodo
    {
        public Nodo Nodo_Izquierdo;
        public int Informacion;
        public Nodo Nodo_Derecho;
        public Nodo()
        {
            this.Nodo_Izquierdo = null;
            this.Informacion = 0;
            this.Nodo_Derecho = null;
        }
    }
}