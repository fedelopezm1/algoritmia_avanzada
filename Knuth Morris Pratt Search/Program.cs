﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kmp
{
    //text search algorithm using KMP
    class KMP
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("");
            Console.WriteLine("Por favor escriba un texto..");
            Console.WriteLine("");
            string text = Console.ReadLine();
            Console.WriteLine("");
            Console.WriteLine($"Ahora escriba los caracteres que deseas buscar en el texto '{text}'");
            Console.WriteLine("");
            string text_search = Console.ReadLine();
            Console.WriteLine("");

            char[] s1 = text.ToCharArray();
            char[] s2 = text_search.ToCharArray();

            KMPSearcher kmp = new KMPSearcher(s2);
            int position = kmp.SearchPosition(s1);

            if (position == -1)
            {
                Console.WriteLine($"No se encontro coincidencias en los caracteres introducidos");
            }
            else
            {
                Console.WriteLine($"Se encontro una coincidencia en la posicion [{position}]");
            }

            Console.ReadKey();
        }
        public class KMPSearcher
        {
            public char[] P;
            public int[] T;

            public KMPSearcher(char[] P)
            {
                this.P = new char[P.Length];
                Array.Copy(P, this.P, P.Length);
                this.T = ComputePrefix(P);
            }
            public int[] ComputePrefix(char[] P)
            {
                int[] result = new int[P.Length];
                int pos = 2;
                int cnd = 0;
                result[0] = -1;
                result[1] = 0;
                while (pos < P.Length)
                {
                    if (P[pos - 1] == P[cnd])
                    {
                        cnd++; result[pos] = cnd; pos++;
                    }
                    else if (cnd > 0)
                    {
                        cnd = result[cnd];
                    }
                    else
                    {
                        result[pos] = 0; pos++;
                    }
                }
                return result;
            }

            public int SearchPosition(char[] S)
            {
                int m = 0;
                int i = 0;
                while (m + i < S.Length)
                {
                    if (this.P[i] == S[m + i])
                    {
                        if (i == this.P.Length - 1)
                            return m;
                        i++;
                    }
                    else
                    {
                        m = m + i - this.T[i];
                        if (this.T[i] > -1)
                            i = this.T[i];
                        else
                            i = 0;
                    }
                }
                return -1;
            }
        }
    }
}
