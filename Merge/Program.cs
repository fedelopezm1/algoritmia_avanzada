﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Merge
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> X = new List<int>(new int[] {2,2,4,4 });
            List<int> Y = new List<int>(new int[] {1,1,3,3 });
            List<int> Z = Merge2(X, Y);
            Console.WriteLine("--Merge --");
            Console.WriteLine("");
            for (int i = 0; i < Z.Count; i++)
                Console.Write(Z[i]);

            Console.ReadKey();
        }

        public static List<int> Merge2(List<int> Array_X, List<int> Array_Y)
        { // este merge los compara y los une  un poco mas organizados
            List<int> New_Node = new List<int>();

            while ((Array_X.Count > 0) || (Array_Y.Count > 0))
            {
                if (Array_X.Count > 0 && Array_Y.Count > 0)
                {
                    if (Array_X.First() >= Array_Y.First())
                    {
                        New_Node.Add(Array_Y.First());
                        Array_Y.Remove(Array_Y.First());
                    }
                    else
                    {
                        New_Node.Add(Array_X.First());
                        Array_X.Remove(Array_X.First());
                    }
                }
                else if (Array_X.Count > 0)
                {
                    New_Node.Add(Array_X.First());
                    Array_X.Remove(Array_X.First());
                }
                else if (Array_Y.Count > 0)
                {
                    New_Node.Add(Array_Y.First());
                    Array_Y.Remove(Array_Y.First());
                }

            }
            return New_Node;
        }
    }
}
